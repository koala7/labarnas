# Labarnas — The final struggle of the Hittite Empire

Narrative board game designed and developed in one weekend during the [Ludum Dare 49](https://ldjam.com/events/ludum-dare/49/labara-a-narrative-board-game-in-ancient-times) game jam.

![hittite-small.png](hittite-small.png)

- narrative board game
- solo play
- takes about 20 minutes
- explore the rules while playing!
- print & play

Simply print out the PDF file and start playing!
Download the [latest version](https://gitlab.com/koala7/labarna/-/raw/master/Labarnas_latest.pdf), print and start playing!


It's my first board game design, feedback is very welcome :)

### Requirements:
 - a deck of 32 standard playing cards
 - one six sided die
 - printed out map
 - printed out counter sheet

__If you want to jump straight ahead, start in section 3 of the rules and immediately start playing while reading along!__


## Gameplay
You have to guide your people through a series of 30 random events. At the start of the game, you will influence the likelihood of certain events in the _Feast for the gods_. But your main task through the game will be the distribution of your population between food production and border defence.

Labarnas is a narrative game. Strategy is involved, but if the gods are not in your favour, there won't be a happy end. So lean back and let the story unfold.

## Setting
The year is about 1200 B.C. You are Šuppiluliuma II., the last _Labarna_ (i.e. king) of the Hittite Empire. The glorious days of your ancestors are past. The extraordinary victory against the Egyptians at Kadesh has deteriorated to being merely a story. Your realm is crumbling.

Nature itself conspires against you. The climate worsened, and your people struggle with droughts and dire straits. They demand better living conditions and must be kept at bay. All the while, the vexatious Assyrians long for the southern territories. Additionally, a new foe, the mysterious Sea People, are raiding your western territories. And if those do not deal the mortal blow, there is always the looming thread of a disastrous catastrophe from the smoking mountain.

But do not despair. Your people are strong and have shaped the region for the better part of a millennium. With a wise leader like you, destiny _can_ be altered.

![cover.jpg](cover.jpg)
